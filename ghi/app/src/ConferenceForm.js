import React, {useEffect, useState} from 'react';
function ConferenceForm(){
    const [locations, setLocations] = useState([])
    const [name, setName] = useState("")
    const [starts, setStarts] = useState("")
    const [ends, setEnds] = useState("")
    const [description, setDescription] = useState("")
    const [maximumPresentations, setMaximumPresentations] = useState("")
    const [maximumAttendees, setMaximumAttendees] = useState("")
    const [location, setLocation]=useState("")
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            setLocations(data.locations)
            console.log(data)
        }
    }
    const handleNameChange = (event) =>{
        const value = event.target.value
        setName(value)
    }
    const handleStartChange = (event) =>{
        const value = event.target.value
        setStarts(value)
    }
    const handleEndChange = (event) =>{
        const value = event.target.value
        setEnds(value)
    }
    const handleDescriptionChange = (event) =>{
        const value = event.target.value
        setDescription(value)
    }
    const handleMaximumPresentationChange = (event) =>{
        const value = event.target.value
        setMaximumPresentations(value)
    }
    const handleMaximumAttendeesChange = (event) =>{
        const value = event.target.value
        setMaximumAttendees(value)
    }
    const handleLocationChange = (event) =>{
        const value = event.target.value
        setLocation(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.starts = starts
        data.ends = ends
        data.location = location
        data.max_presentations = maximumPresentations
        data.max_attendees = maximumAttendees
        data.description = description
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok){
            const newConference = await response.json()
            setName('')
            setDescription('')
            setStarts('')
            setEnds('')
            setLocation('')
            setMaximumAttendees('')
            setMaximumPresentations('')
        }

    }
useEffect(() =>{
    fetchData();
},[])


return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form  onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value = {name} onChange={handleNameChange} placeholder="Name" required type="text" name = "name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value ={starts} onChange={handleStartChange} placeholder="starts" required type="date" name = "starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value ={ends} onChange={handleEndChange} placeholder="Ends" required type="date" name = "ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div><h5>Description</h5></div>
              <div className="form-floating mb-3">
                <textarea value ={description} onChange={handleDescriptionChange} rows = "5" cols = "66" name = "description" id= "description" className = "form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value ={maximumPresentations} onChange={handleMaximumPresentationChange} placeholder="maximum presentation" required type="number" name = "max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="maximum presentation">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value = {maximumAttendees} onChange={handleMaximumAttendeesChange}  placeholder="maximum attendees" required type="number" name = "max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="maximum attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value = {location} onChange={handleLocationChange} required id="location" name = "location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location =>{
                    return(
                        <option key ={location.href} value = {location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
)
}
export default ConferenceForm
