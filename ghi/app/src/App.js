import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendeeForm';
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from 'react-router-dom';
import PresentationForm from './new-presentation';
import MainPage from './MainPage'


function App(props) {
 
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage/>} />
          <Route path="locations">
            <Route path="new" element={<LocationForm/>} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList/>}/>
            <Route path="new" element={<AttendConferenceForm/>}/>
          </Route>
          <Route path="conference">
            <Route path="new" element={<ConferenceForm/>}/>
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
